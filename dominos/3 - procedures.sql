CREATE OR REPLACE FUNCTION standardizeTelefoonnummer(
  telefoonnummer IN VARCHAR2
) RETURN VARCHAR2 AS 
BEGIN
  RETURN REPLACE(REPLACE(telefoonnummer, '-', ''), ' ', '');
END standardizeTelefoonnummer;
/


CREATE OR REPLACE FUNCTION standardizePostcode(
  postcode IN VARCHAR2
) RETURN VARCHAR2 AS 
BEGIN
  /*
   *  - make upper case
   *  - remove any spaces 
   */
  RETURN REPLACE(UPPER(postcode), ' ', '');
END standardizePostcode;
/


CREATE OR REPLACE FUNCTION generatePostcodeId(
  postcode IN VARCHAR2
) RETURN VARCHAR2 AS 
BEGIN
  RETURN CAST(SUBSTR(postcode, 1, 4) || ASCII(SUBSTR(postcode, 5, 1)) || ASCII(SUBSTR(postcode, 6, 1)) AS NUMBER);
END generatePostcodeId;
/


CREATE OR REPLACE FUNCTION findPostcodeHuisnummerId(
  postcodeId IN NUMBER,
  huisnummer IN VARCHAR2
) RETURN VARCHAR2 IS
  tableId NUMBER;
  dashPosition NUMBER;
  leastHuisnummer NUMBER;
  greatestHuisnummer NUMBER;
  expectedNumberType VARCHAR2(5);
BEGIN
  dashPosition := INSTR(huisnummer, '-');
  
  IF (dashPosition > 0) THEN
    leastHuisnummer := CAST(SUBSTR(huisnummer, 1, dashPosition - 1) AS NUMBER);
    greatestHuisnummer := CAST(SUBSTR(huisnummer, dashPosition + 1) AS NUMBER);
  ELSE
    leastHuisnummer := CAST(huisnummer AS NUMBER);
    greatestHuisnummer := leastHuisnummer;
  END IF;

  -- Depending on the mod of the huisnummers, we expect a number type.
  IF (MOD(leastHuisnummer, 2) != MOD(greatestHuisnummer, 2)) THEN
    expectedNumberType := 'mixed';
  ELSIF (MOD(leastHuisnummer, 2) = 0) THEN
    expectedNumberType := 'even';
  ELSE
    expectedNumberType := 'odd';
  END IF;
  
  BEGIN
    SELECT
      ID
    INTO
      tableId
    FROM 
      POSTCODE 
    WHERE 
      POSTCODE_ID = postcodeId
    AND
      MINNUMBER <= leastHuisnummer
    AND
      MAXNUMBER >= greatestHuisnummer
    AND (
        -- this might end up as NUMBERTYPE = 'mixed' OR NUMBERTYPE = 'mixed', but the results are correct
        NUMBERTYPE = 'mixed' 
      OR 
        NUMBERTYPE = expectedNumberType
    )
    AND
      -- We're lazy and want to SELECT INTO without a cursor. Normally the table should have a unique key for postcode + huisnummer range, but this one doesn't
      ROWNUM <= 1;
  EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR (-20001, 'Postcode id en huisnummer ' || postcodeId || ' ' || leastHuisnummer || '-' || greatestHuisnummer || ' konden niet worden gevonden in de database');
  END;
  
  RETURN tableId;
END findPostcodeHuisnummerId;
/


CREATE OR REPLACE PROCEDURE parseHuisnummerToevoeging(
   huisnummerString IN VARCHAR2,
   huisnummer OUT VARCHAR2,
   toevoeging OUT VARCHAR2
) IS
  dashPosition NUMBER;
BEGIN
  /*
   * Huisnummer parsing/sanitization
   * 
   * Huisnummers is varchar2 because it can contain 2 huisnummers ('10-12' or '10/12')
   * 
   *  - replace '/' with '-'
   *  - if '-' exists, leastHuisnummer and greatestHuisnummer will become 2 values, otherwise they are the same
   */
  toevoeging := TRIM(REGEXP_SUBSTR(huisnummerString, '[A-Za-z]+$| [0-9A-Za-z]$'));
  
  IF (toevoeging IS NULL) THEN
    huisnummer := REPLACE(huisnummerString, '/', '-');
  ELSE
    huisnummer := REPLACE(SUBSTR(huisnummerString, 1, LENGTH(huisnummerString) - LENGTH(toevoeging)), '/', '-');
  END IF;
  
END parseHuisnummerToevoeging;
/


CREATE OR REPLACE PROCEDURE newWinkel(
  naam IN WINKEL.NAAM%TYPE,
  postcode IN WINKEL.POSTCODE%TYPE,
  huisnummer IN WINKEL.HUISNR%TYPE,
  toevoeging IN WINKEL.TOEVOEGING%TYPE,
  telefoonnummer IN WINKEL.TELNR%TYPE
) IS
  postcodeId NUMBER;
  postcodeTableId NUMBER;
BEGIN
  -- generate postcode id
  postcodeId := generatePostcodeId(postcode);
  
  -- gets the id of the matching postcode entry in the table
  postcodeTableId := findPostcodeHuisnummerId(postcodeId, huisnummer);
  
  BEGIN
    INSERT INTO
      WINKEL(
        POSTCODE_ID_FK,
        NAAM,
        POSTCODE,
        POSTCODE_ID,
        HUISNR,
        TOEVOEGING,
        TELNR,
        OPGEHEVEN
      ) VALUES (
        postcodeTableId,
        naam,
        postcode,
        postcodeId,
        huisnummer,
        toevoeging,
        telefoonnummer,
        0
      );
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN
        DBMS_OUTPUT.PUT_LINE('Skipping winkel because duplicate');
      WHEN OTHERS THEN
        RAISE;
    END;
END newWinkel;
/


CREATE OR REPLACE PROCEDURE changeWinkel(
  winkelId IN WINKEL.ID%TYPE,
  postcode IN WINKEL.POSTCODE%TYPE,
  huisnummer IN WINKEL.HUISNR%TYPE,
  toevoeging IN WINKEL.TOEVOEGING%TYPE,
  telefoonnummer IN WINKEL.TELNR%TYPE
) IS
  postcodeId NUMBER;
  postcodeTableId NUMBER;
  tmpToevoeging WINKEL.TOEVOEGING%TYPE;
BEGIN
  -- generate postcode id
  postcodeId := generatePostcodeId(postcode);
  
  -- gets the id of the matching postcode entry in the table
  postcodeTableId := findPostcodeHuisnummerId(postcodeId, huisnummer);
  
  tmpToevoeging := toevoeging;
  
  BEGIN
    UPDATE
      WINKEL
    SET
      POSTCODE_ID_FK = postcodeTableId,
      POSTCODE = postcode,
      HUISNR = huisnummer,
      TOEVOEGING = tmpToevoeging,
      TELNR = telefoonnummer
    WHERE
      ID = winkelId;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Cannot update, winkel with id ' || winkelId || ' could not be found');
      WHEN DUP_VAL_ON_INDEX THEN
        DBMS_OUTPUT.PUT_LINE('Skipping winkel because of a duplicate value for a unique constraint');
      WHEN OTHERS THEN
        RAISE;
    END;
END changeWinkel;
/


CREATE OR REPLACE FUNCTION zoekWinkelsInProvincie(
  provincie IN POSTCODE.PROVINCE%TYPE
)
RETURN SYS_REFCURSOR
AS
  returnCursor SYS_REFCURSOR;
BEGIN
  OPEN returnCursor FOR
    SELECT 
      WINKEL.* 
    FROM 
      WINKEL
    INNER JOIN
      POSTCODE
    ON
      WINKEL.POSTCODE_ID_FK = POSTCODE.ID
    WHERE
      UPPER(POSTCODE.PROVINCE) = UPPER(provincie);
  
  RETURN returnCursor;
END zoekWinkelsInProvincie;
/


CREATE OR REPLACE FUNCTION zoekAdres(
  postcode IN VARCHAR2,
  huisnummer IN NUMBER,
  postcodeTableId OUT POSTCODE.ID%TYPE,
  straat OUT POSTCODE.STREET%TYPE,
  plaats OUT POSTCODE.CITY%TYPE,
  provincie OUT POSTCODE.PROVINCE%TYPE
) RETURN BOOLEAN IS
  postcodeId NUMBER;
BEGIN
  postcodeId := generatePostcodeId(postcode);
  
  BEGIN
    postcodeTableId := findPostcodeHuisnummerId(postcodeId, huisnummer);
  
    SELECT
      STREET,
      CITY,
      PROVINCE
    INTO
      straat,
      plaats,
      provincie
    FROM
      POSTCODE
    WHERE
      ID = postcodeTableId
    AND
      ROWNUM <= 1;
      
    RETURN TRUE;
      
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN FALSE;
    WHEN OTHERS THEN
      RAISE;
  END;

END zoekAdres;
/


CREATE OR REPLACE FUNCTION calculateGeoDistance(
  lat1 IN NUMBER,
  lon1 IN NUMBER,
  lat2 IN NUMBER,
  lon2 IN NUMBER
) RETURN NUMBER IS
  degToRad NUMBER := 57.29577951;
  radius NUMBER := 6387700;
BEGIN
  -- original from https://www.geodatasource.com/developers/sql
  RETURN(
    NVL(radius, 0) 
    * 
    ACOS(
      (
        SIN(NVL(lat1, 0) / degToRad) 
        * SIN(NVL(lat2, 0) / degToRad)
      ) 
      + 
      (
        COS(NVL(lat1, 0) / degToRad)
        * COS(NVL(lat2, 0) / degToRad)
        * COS(NVL(lon2, 0) / degToRad - NVL(lon1, 0) / degToRad)
      )
    )
  );
END calculateGeoDistance;
/


CREATE OR REPLACE PROCEDURE zoekWinkelVoorAdres(
  postcode IN VARCHAR2,
  huisnummer IN VARCHAR2,
  winkelId OUT WINKEL.ID%TYPE,
  distance OUT NUMBER
) IS
  postcodeId NUMBER;
  postcodeLat NUMBER;
  postcodeLon NUMBER;
  postcodeTableId NUMBER;
BEGIN
  postcodeId := generatePostcodeId(postcode);
  postcodeTableId := findPostcodeHuisnummerId(postcodeId, huisnummer);

  -- Get the lat/lon for the entered postcode
  SELECT
    LAT,
    LON
  INTO
    postcodeLat,
    postcodeLon
  FROM 
    POSTCODE
  WHERE
    ID = postcodeTableId;
    
  -- get the id/distance (in metres) for the closest store
  SELECT 
    WINKEL_ID,
    DISTANCE
  INTO
    winkelId,
    distance
  FROM (
    SELECT
      WINKEL.ID AS WINKEL_ID,
      calculateGeoDistance(LAT, LON, postcodeLat, postcodeLon) AS DISTANCE
    FROM 
      WINKEL 
    INNER JOIN
      POSTCODE
    ON
      WINKEL.POSTCODE_ID_FK = POSTCODE.ID
    ORDER BY DISTANCE ASC
  )
  WHERE
    ROWNUM <= 1;
     
END zoekWinkelVoorAdres;
/


CREATE OR REPLACE PROCEDURE importWinkels IS
  fileHandle UTL_FILE.FILE_TYPE;
  buffer VARCHAR(8192);
  naam WINKEL.NAAM%TYPE;
  straat VARCHAR2(256);
  huisnummerString VARCHAR2(10);
  plaats VARCHAR(32);
  land VARCHAR(3);
  postcodeString VARCHAR(10);
  telefoonnummerString VARCHAR(20);
  
  postcode WINKEL.POSTCODE%TYPE;
  huisnummer WINKEL.HUISNR%TYPE;
  toevoeging WINKEL.TOEVOEGING%TYPE;
  telefoonnummer WINKEL.TELNR%TYPE;
  
  lineNumber NUMBER;
BEGIN
  -- Start reading
  lineNumber := 0;
  fileHandle := UTL_FILE.FOPEN('DOMINOS_ROOT', 'winkels.txt', 'R');

  LOOP  
    BEGIN    
      UTL_FILE.GET_LINE(fileHandle, buffer);
      naam := TRIM(buffer);
      lineNumber := lineNumber + 1;
       
      IF (TRIM(naam) IS NULL) THEN
        CONTINUE;
      END IF;
        
      UTL_FILE.GET_LINE(fileHandle, buffer);
      lineNumber := lineNumber + 1;
      straat := TRIM(buffer);
      
      UTL_FILE.GET_LINE(fileHandle, buffer);
      lineNumber := lineNumber + 1;
      huisnummerString := TRIM(buffer);
      
      UTL_FILE.GET_LINE(fileHandle, buffer);
      lineNumber := lineNumber + 1;
      plaats := TRIM(buffer);
      
      UTL_FILE.GET_LINE(fileHandle, buffer);
      lineNumber := lineNumber + 1;
      land := TRIM(buffer);
      
      UTL_FILE.GET_LINE(fileHandle, buffer);
      lineNumber := lineNumber + 1;
      postcodeString := TRIM(buffer);
      
      UTL_FILE.GET_LINE(fileHandle, buffer);
      lineNumber := lineNumber + 1;
      telefoonnummerString := TRIM(buffer);
      
      -- No checking logic, only parsing of the file
      telefoonnummer := standardizeTelefoonnummer(telefoonnummerString);
      postcode := standardizePostcode(postcodeString);
      parseHuisnummerToevoeging(huisnummerString, huisnummer, toevoeging);
      
      -- add the winkel (newWinkel checks the correctness of the data)
      newWinkel(naam, postcode, huisnummer, toevoeging, telefoonnummer);
      
      DBMS_OUTPUT.PUT_LINE('Added winkel ' || naam);
        
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          EXIT;
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('Fout op regel ' || lineNumber || ' van het tekstbestand: ' || SQLERRM());
          RAISE;
    END;
   END LOOP;
  UTL_FILE.FCLOSE(fileHandle);

END importWinkels;
/