-- Zet de server output aan
set serveroutput on

-- Maak de landen tabel
CREATE TABLE Country
(
  ID NUMBER PRIMARY KEY,
  -- Edited door Gerrit: Code is maar 2 chars lang normaliter
  Code VARCHAR2(2) UNIQUE NOT NULL,
  Name VARCHAR2(50) UNIQUE NOT NULL
);

-- Maak de universiteiten tabel
CREATE TABLE University
(
  ID NUMBER PRIMARY KEY,
  Code VARCHAR2(4) UNIQUE NOT NULL,
  CountryID NUMBER NOT NULL,
  Name VARCHAR2(100) NOT NULL,
  SNL NUMBER NOT NULL,
  
  FOREIGN KEY (CountryID) REFERENCES Country(ID)
);

-- Maak de studenten tabel
CREATE TABLE Student
(
  StudentNumber NUMBER PRIMARY KEY,
  Name		    VARCHAR2(30) UNIQUE NOT NULL,
  University_id	NUMBER UNIQUE NOT NULL,

  FOREIGN KEY (University_id) REFERENCES University(ID)
);

-- Voeg wat landen toe
INSERT INTO Country VALUES (1, 'NL', 'The Netherlands');
INSERT INTO Country VALUES (2, 'FR', 'France');
INSERT INTO Country VALUES (3, 'DE', 'Germany');
INSERT INTO Country VALUES (4, 'GB', 'United Kingdom');
INSERT INTO Country VALUES (5, 'BE', 'Belgium');
INSERT INTO Country VALUES (6, 'US', 'United States of America');
INSERT INTO Country VALUES (7, 'CA', 'Canada');

-- Universiteiten in Gelderland
INSERT INTO University VALUES (0, 'TUA',   1, 'Theologische Universiteit Apeldoorn', 8);
INSERT INTO University VALUES (1, 'RUN',   1, 'Radboud Universiteit Nijmegen', 10);
INSERT INTO University VALUES (2, 'WUR',   1, 'Wageningen Universiteit', 10);

-- Universiteiten in Groningen
INSERT INTO University VALUES (3, 'RUG',   1, 'Rijksuniversiteit Groningen', 7);

-- Universiteiten in Limburg
INSERT INTO University VALUES (4, 'UM',    1, 'Universiteit Maastricht', 8);
INSERT INTO University VALUES (5, 'OU',    1, 'Open Universiteit', 7);
INSERT INTO University VALUES (6, 'MSM',   1, 'Maastricht School of Management', 10);

-- Universiteiten in Noord-Brabant
INSERT INTO University VALUES (7, 'TUE',   1, 'Technische Universiteit Eindhoven', 9);
INSERT INTO University VALUES (8, 'TIU',   1, 'Universiteit van Tilburg', 9);

-- Universiteiten in Noord-Holland
INSERT INTO University VALUES (9, 'UVA',   1, 'Universiteit van Amsterdam', 10);
INSERT INTO University VALUES (10, 'VU',   1, 'Vrije Universiteit', 9);

-- Universiteiten in Overijssel
INSERT INTO University VALUES (11, 'PTHU', 1, 'Protestantse Theologische Universiteit vestiging Kampen', 7);
INSERT INTO University VALUES (12, 'TUK',  1, 'Theologische Universiteit Kampen', 6);
INSERT INTO University VALUES (13, 'UT',   1, 'Universiteit Twente', 7);

-- Universiteiten in Utrecht
INSERT INTO University VALUES (14, 'UU',   1, 'Universiteit Utrecht', 8);
INSERT INTO University VALUES (15, 'NBU',  1, 'Nyenrode Business Universiteit', 10);
INSERT INTO University VALUES (16, 'UVH',  1, 'Universiteit voor Humanistiek', 6);
INSERT INTO University VALUES (17, 'KTU',  1, 'Katholieke Theologische Universiteit', 6);
INSERT INTO University VALUES (18, 'TIAS', 1, 'TiasNimbas Business School', 7);

-- Universiteiten in Zeeland
INSERT INTO University VALUES (19, 'UCR',  1, 'University College Roosevelt', 7);

-- Universiteiten in Noord-Holland
INSERT INTO University VALUES (20, 'TUD',  1, 'Technische Universiteit Delft', 9);
INSERT INTO University VALUES (21, 'UL',   1, 'Universiteit Leiden', 10);
INSERT INTO University VALUES (22, 'RUR',  1, 'Erasmus Universiteit Rotterdam', 10);

-- University of Phoenix
INSERT INTO University VALUES (23, 'UOPX',  1, 'University of Phoenix', 30);

-- Voeg enkele studenten toe
INSERT INTO Student VALUES (123456789, 'Pietje Puk', 7);
INSERT INTO Student VALUES (2345678, 'Jan Klaassen', 3);
INSERT INTO Student VALUES (345678, 'Donald Duck', 12);
INSERT INTO Student VALUES (1111111111, 'Willie Wortel', 9);
INSERT INTO Student VALUES (666666, 'Tom Buis', 17);

-- Een hulp functie die bij de test cases wordt gebruik
CREATE OR REPLACE PROCEDURE ASSERT_EQUALS (
  actual VARCHAR2,
  expected VARCHAR2
)
AS
BEGIN
  IF (NVL(actual, -1) ^= NVL(expected, -2)) THEN
    RAISE_APPLICATION_ERROR(-20000, 'ASSERT FAILS. ' || actual || ' != ' || expected);
  END IF;
END;
/

-- BIG MOD FUNCTION
CREATE OR REPLACE FUNCTION BIGMOD(
  n IN VARCHAR2,
  d IN NUMBER
) RETURN NUMBER IS
  part NUMBER;
  partLength NUMBER;
  partMod NUMBER;
  workingNumber VARCHAR2(32767);
BEGIN
  
  workingNumber := n;
  
  WHILE (LENGTH(workingNumber) > 9)
  LOOP
    part := CAST(SUBSTR(workingNumber, 1, 9) AS NUMBER);
    partMod := MOD(part, d);
    
    workingNumber := partMod || SUBSTR(workingNumber, 10);
  END LOOP;
  
  RETURN MOD(CAST(SUBSTR(workingNumber, 1) AS NUMBER), d);
END BIGMOD;
/


-- generateIsin
CREATE OR REPLACE FUNCTION generateIsin 
(
  countryCode IN COUNTRY.CODE%TYPE,
  universityCode IN UNIVERSITY.CODE%TYPE,
  studentNumber IN VARCHAR2
) RETURN VARCHAR2 IS
  countryId NUMBER;
  studentNumberLength NUMBER;
  letterPart VARCHAR(6);
  letterPartLength NUMBER;
  numericLetterPart VARCHAR2(12);
  temp VARCHAR2(42);
  tempLength NUMBER;
  tempParts NUMBER;
  partLength NUMBER;
  dividend VARCHAR2(52);
  checksum NUMBER;
  isin VARCHAR(47);
  i NUMBER;
  j NUMBER;
  
  countryNotFound EXCEPTION;
  universityNotExists EXCEPTION;
  invalidStudentNumber EXCEPTION;
BEGIN
  -- todo: check if universityCode and countryCode exist
  BEGIN
    SELECT ID INTO countryId FROM COUNTRY WHERE CODE = countryCode;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE countryNotFound;
  END;
  
  BEGIN
    SELECT SNL into studentNumberLength FROM UNIVERSITY WHERE COUNTRYID = countryId AND CODE = universityCode;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE universityNotExists;
  END;
  
  IF (LENGTH(studentNumber) != studentNumberLength) THEN
    RAISE invalidStudentNumber;
  END IF;

  letterPart := UPPER(universityCode) || UPPER(countryCode);
  letterPartLength := LENGTH(letterPart);

  FOR i IN 1..letterPartLength
  LOOP
    -- University code is always letters, letters are always ASCII. 
    -- Specs say 'A' = 16, 'B' = 17, etc. 'A' has ASCII value 65, 65 - 49 = 16
    -- to be certain we convert to uppercase first
    numericLetterPart := numericLetterPart || ASCII(SUBSTR(letterPart, i, 1)) - 49;
  END LOOP;
  
  temp := numericLetterPart || studentNumber;
  
  tempLength := LENGTH(temp);
  tempParts := CEIL(tempLength / 4);
    
  FOR i IN 1..tempParts
  LOOP
    IF (i * 4 > tempLength) THEN
        partLength := MOD(tempLength, 4);
      ELSE
        partLength := 4;
    END IF;
    
    FOR j IN REVERSE (((i - 1) * 4) + 1)..(((i - 1) * 4) + partLength)
    LOOP
      dividend := SUBSTR(temp, j, 1) || dividend;
    END LOOP;
  END LOOP;
    
  checksum := BIGMOD(dividend, 62);
  
  temp := studentNumber || LPAD(checksum, 2, '0');
  tempLength := LENGTH(temp);
  
  isin := countryCode;
  
  FOR i IN 1..tempLength
  LOOP
    IF (MOD(i - 1, 4) = 0) THEN
      isin := isin || ' ';
    END IF;
    
    isin := isin || SUBSTR(temp, i, 1);
  END LOOP;
  
  isin := isin || ' ' || universityCode;
  
  RETURN isin;
END generateIsin;
/

BEGIN
  ASSERT_EQUALS(generateISIN('NL','TUA','98162670'),'NL 9816 2670 60 TUA');
  ASSERT_EQUALS(generateISIN('NL','RUN','1889924721'),'NL 1889 9247 2130 RUN');
  ASSERT_EQUALS(generateISIN('NL','WUR','8760503442'),'NL 8760 5034 4210 WUR');
  ASSERT_EQUALS(generateISIN('NL','RUG','7948372'),'NL 7948 3725 2 RUG');
  ASSERT_EQUALS(generateISIN('NL','UM','10392781'),'NL 1039 2781 20 UM');
  ASSERT_EQUALS(generateISIN('NL','OU','2551716'),'NL 2551 7166 0 OU');
  ASSERT_EQUALS(generateISIN('NL','MSM','5337149774'),'NL 5337 1497 7446 MSM');
  ASSERT_EQUALS(generateISIN('NL','TUE','543595679'),'NL 5435 9567 926 TUE');
  ASSERT_EQUALS(generateISIN('NL','TIU','498476766'),'NL 4984 7676 634 TIU');
  ASSERT_EQUALS(generateISIN('NL','UVA','5389537253'),'NL 5389 5372 5343 UVA');
  ASSERT_EQUALS(generateISIN('NL','VU','658372658'),'NL 6583 7265 832 VU');
  ASSERT_EQUALS(generateISIN('NL','PTHU','7688668'),'NL 7688 6682 7 PTHU');
  ASSERT_EQUALS(generateISIN('NL','TUK','429859'),'NL 4298 5932 TUK');
  ASSERT_EQUALS(generateISIN('NL','UT','7279553'),'NL 7279 5534 5 UT');
  ASSERT_EQUALS(generateISIN('NL','UU','04692710'),'NL 0469 2710 10 UU');
  ASSERT_EQUALS(generateISIN('NL','NBU','1100500646'),'NL 1100 5006 4613 NBU');
  ASSERT_EQUALS(generateISIN('NL','UVH','862176'),'NL 8621 7619 UVH');
  ASSERT_EQUALS(generateISIN('NL','KTU','903151'),'NL 9031 5123 KTU');
  ASSERT_EQUALS(generateISIN('NL','TIAS','7294147'),'NL 7294 1472 8 TIAS');
  ASSERT_EQUALS(generateISIN('NL','UCR','7187419'),'NL 7187 4195 8 UCR');
  ASSERT_EQUALS(generateISIN('NL','TUD','548194600'),'NL 5481 9460 022 TUD');
  ASSERT_EQUALS(generateISIN('NL','UL','7906541256'),'NL 7906 5412 5643 UL');
  ASSERT_EQUALS(generateISIN('NL','RUR','6396609648'),'NL 6396 6096 4834 RUR');
  ASSERT_EQUALS(generateISIN('US','UOPX','301522821691826471941782343133'),'US 3015 2282 1691 8264 7194 1782 3431 3322 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','523595200069997478468516090574'),'US 5235 9520 0069 9974 7846 8516 0905 7432 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','582252573807395057043924916248'),'US 5822 5257 3807 3950 5704 3924 9162 4822 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','885212525407281230171639341071'),'US 8852 1252 5407 2812 3017 1639 3410 7108 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','246082509850885817941238878858'),'US 2460 8250 9850 8858 1794 1238 8788 5854 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','254813395865332527223319330076'),'US 2548 1339 5865 3325 2722 3319 3300 7614 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','650175233613662342022245787007'),'US 6501 7523 3613 6623 4202 2245 7870 0740 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','769930711156115905628621556591'),'US 7699 3071 1156 1159 0562 8621 5565 9130 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','944832934567761037045517968994'),'US 9448 3293 4567 7610 3704 5517 9689 9414 UOPX');
  ASSERT_EQUALS(generateISIN('US','UOPX','856563611608570565634652119720'),'US 8565 6361 1608 5705 6563 4652 1197 2018 UOPX');
END;


